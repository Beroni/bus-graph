// TODO
$(document).ready(() => {
    getGraphData();
})
var labelNode = [];
var latlngNode = [];
function getNodes(){

    var itemsProcessed = 0;

    $.ajax({
        type: 'GET',
        url:'/getNodes',
        dataType: 'json',
        success: function(data){
            console.log(data.length);
            nodeData = data;
            console.log("Funcionou");
            data.forEach(element => {
                labelNode.push(element.name);
                latlngNode.push([element.lat,element.long]);
                L.marker([element.lat,element.long],{
                icon: icone
                }).addTo(map);
                itemsProcessed+=1;
                if(itemsProcessed === data.length){
                    getEgdes();
                }
            });
        },
        error: function(){
            console.error("Não funcionou");
        }
    });
}

function getEgdes(){
    $.ajax({
        type: 'GET',
        url:'/getEdges',
        dataType: 'json',
        success: function(data){
            console.log("Funcionou");
            console.log(data.length);
            data.forEach(element => {
                var first_node = labelNode.indexOf(element[0]);
                var second_node = labelNode.indexOf(element[1]);
                if(!(latlngNode[first_node] === undefined || latlngNode[second_node] === undefined)){
                L.polyline([ L.latLng(latlngNode[first_node][0],latlngNode[first_node][1]), L.latLng(latlngNode[second_node][0],latlngNode[second_node][1])],{
                    color: 'blue'
                }).addTo(map);
                }
            });
        },
        error: function(){
            console.error("Não funcionou");
        }
    });
}

function getGraphData(){
    $.ajax({
        type: 'GET',
        url:'/data',
        dataType: 'json',
        success: function(data){
            let list = [];
            $.each(data, (key, value) => {
                list.push({type: key, value: value});
            });
            $("data").html(componentGraph(list, "dataGraph"));
        }
    });
}

function configDescription(data){
    resetDescription()
    console.log(componentGraph([{}],`${data}_graph`))
    if(data.includes("Matriz")){
        $(`#${data}_box`).addClass('matriz');
    }else if(data.includes("List")){
        $(`#${data}_box`).addClass('list');
    } else{
        $(`#${data}_box`).addClass('active');
    }
    setTimeout(function(){
        $(`#${data}_description`).show().html(componentGraph([{}],`${data}_graph`))
    }, 400);


    $(`#${data}_square`).attr('onclick', "resetDescription()");
}

function resetDescription(){
    console.log("asdasdad")
    let box = ["vetores", "arestas","MaiorGrau","Ordem", "Tamanho", "Tipo", "Matriz", "Lista"];
    box.forEach((key, value)=>{

        $(`#${key}_box`).removeClass("active");
        $(`#${key}_box`).removeClass("list");
        $(`#${key}_box`).removeClass("matriz");
        $(`#${key}_square`).attr('onclick', `configDescription('${key}')`);
        $(`#${key}_description`).hide();
    });
}


let component = {
    dataGraph: ({type, value}) => `
        <div id="${type}_square" class="Retngulo-32" onclick="configDescription('${type}')">
            <div id="${type}_box" class="data-type">
                <span> ${type}</span>
                <span id="${type}_description"></span>
            </div>
            <div class="data-value">
                <span>${value}</span>
            </div>
        </div>
    `,
    vetores_graph: ({}) => `
    <div style="padding: 10px">
        <p >
            Podem representar dados de todo o mundo como por exemplo, Cidades, pessoas, máquinas,números, etc 
            nesse caso existem 4761 pontos de ônibus em fortaleza.
        </p>
    </div>
    `,
    arestas_graph: ({}) =>`
        <div style="padding: 10px">
           <p>
           Existência de ligações entre nós, valor da ligação entre nós, distância entre nós, etc 
           </p>
           <p >
            Existem 5901 arestas.
           </p>
       </div>
    `,
    MaiorGrau_graph: ({}) => `

    <div style="padding: 10px">
        <p >o maior grau é represtado pelo terminal do siqueira com 17 ligações.</p>
    </div>
    `,
    Ordem_graph: ({}) => `
    <div style="padding: 10px">
        <p >Ordem de um grafo é o número de vetores do mesmo, nesse caso 4761</p>
    </div>
    `,
    Tamanho_graph: ({}) => `
    <div style="padding: 10px">
        <p >Tamanho de um Grafo é dado pela Soma  de V + A, vertices + arestas, e nesse caso é igual a 10662</p>
    </div>
    `,
    Tipo_graph: ({}) => `
    <div style="padding: 10px">
        <p>Ponderado, simples, rotulado, não-planar, dirigido, com sub-grafos, Ciclos e circuitos, distribuição não-aleatoria.</p>
    </div>
    `,
    Matriz_graph: ({}) =>`
        <img src="../img/matriz.png"/>
    `,
    Lista_graph: ({}) =>`
        <img src="../img/list.png"/>
    `
}

function componentGraph(data, which){
    let html = data.map(component[which]).join('');
    console.log(html)
    return html;
}

