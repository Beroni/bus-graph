var map = L.map('map-id').setView([-3.7608046,-38.5496377],12)

L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
    maxZoom: 18,
    bounds:[
        new L.LatLng(-3.621001, -38.397255),
        new L.LatLng(-3.914258, -38.740641)
    ]
}).addTo(map);



var icone = L.icon({
    iconUrl: 'img/circle.png',
    iconSize:     [4, 4] 
});


getNodes();



