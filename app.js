const express = require('express');
const engines = require('consolidate');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');

const PORT = process.env.PORT || 3000;
app.use(bodyParser.json());// support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static('public'));
app.engine('html', engines.mustache);
app.set('view engine', 'html');

app.get('/getNodes',function(req, res){
    let list = Graph.filter(Graph.get());
    let nodes = Nodes.get(list["nodes"]);
    let edges = Edges.get(list["edges"]);
    let nodes_lat_long = Nodes.getLatLong(list["nodes"]);
    console.log(nodes.length);
    res.send(nodes_lat_long);
});

app.get('/getEdges',function(req, res){
    let list = Graph.filter(Graph.get());
    let edges = Edges.get(list["edges"]);
    res.send(edges);
});

app.get('/',function(req,res){
    res.render(__dirname+"/index.html");
});

app.get('/beroni',function(req,res){
    let list = Graph.filter(Graph.get());
    let nodes = Nodes.get(list["nodes"]);
    let edges = Edges.get(list["edges"]);
    res.send(DataStruct.matriz(nodes, edges));
});

app.get('/data',function(req,res){
    let list = Graph.filter(Graph.get());
    let nodes = Nodes.get(list["nodes"]);
    let edges = Edges.get(list["edges"]);
    let graph = {
        vetores : nodes.length,
        arestas : edges.length,
        Matriz: "Adjacencia",
        Lista: "Adjacencia",
        MaiorGrau : "Siqueira " +Nodes.NodesDegree(DataStruct.list(nodes, edges))[0][1],
        Tamanho : nodes.length + edges.length,
        Ordem : nodes.length,
        Tipo: "Direcionado"
    };
    res.send(graph);
});

const Graph = {
    get: ()=> {
        const file = './public/file/bus-network.txt';
        const graph = fs.readFileSync(file, 'utf8').toString().split("\n");
        return graph;
    },
    filter: (graph) => {
        let node = false;
        let edge = false;
        let nodes = [];
        let edges = [];
        graph.forEach((key) => {
            if(key.includes("edges") || edge === true){
                edge = true;
                edges.push(key);
            }else if(key.includes("nodes") || node === true){
                node = true;
                nodes.push(key);
            }
        });
        graph = {
            nodes: nodes,
            edges: edges
        };
        return graph;
    }
};

const Nodes = {
    get: (data) => {
        data.shift();
        let nodes = [];
        data.forEach((node)=>{
            let node_name = node.split(",")[0];
            nodes.push(node_name);
        });
        return nodes.sort();
    },
    getLatLong: (data) => {
        let nodes = [];
        data.forEach((node)=>{
            node = node.split(",");
            let node_filted = {
                name: node[0],
                lat: node[8],
                long: node[6]
            };
            nodes.push(node_filted);
        });
        return nodes.sort((a, b) => {
           if(a.name < b.name) return -1;
           if(a.name > b.name) return 1;
           return 0;
        });
    },
    NodesDegree: (list) => {
        let degree = []
        for (var [key, value] of Object.entries(list)) {
            degree.push([key, value.length]);

        }

        return degree.sort((a, b) => (b[1] - a[1]));
    }
};

const Edges = {
    get: (data) => {
        data.shift();
        let edges = [];
        data.forEach((edge) => {
           edge = edge.split(",");
           edges.push([edge[0], edge[2]]);
        });
        return edges;
    }
}

const DataStruct = {
    matriz : (nodes = [1,2,3], edges = [[1,2],[2,3]]) => {
        let nodesLength = nodes.length;
        let matrix = Array(nodesLength).fill(0).map(x => Array(nodesLength).fill(0));
        edges.forEach((edge) => {
            let indexX = nodes.indexOf(edge[0]);
            let indexY = nodes.indexOf(edge[1]);

            matrix[indexX][indexY] += 1;
        });
        return matrix
    },
    list : (nodes = [1,2,3], edges = [[1,2],[2,3]]) =>{
        let list = nodes.reduce((obj, val) => { obj[val] = []; return obj;}, {});
        edges.forEach((edge) => {
            list[edge[0]].push(edge[1]);
        });
        return list
    }
};

app.listen(PORT);